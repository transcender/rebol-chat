Rebol [
    title: "Register with chat server"
    author: "Graham"
    date: 6-Feb-2020
    version: 0.0.4
    notes: {runs from Rebol console}
]

chat-server: http://35.224.174.22

register: func [chat-server [url!]
    <local> def link
][
    chat-server: dirize chat-server
    def: _
    net-trace off
    print ["The registration process needs to get your public key, and your details encrypted with the server's public key."]
    print spaced ["Example:" https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/users/graham.md]
    cycle [
        print "URL for your public key and encrypted registration details"
        if not empty? def [print unspaced [{Use d for "} def {"}]]
        prin "Q to quit: " link: input
        if link = "q" [halt]
        if link = "d" [link: copy def] else [def: copy link]
        if empty? link [continue]
        if parse link [ "https://" thru "raw" to end][
            ; got a raw github/gitlab link, so let's continue
            attempt [
                if not exists? link: to url! link [print spaced ["this link" link "doesn't resolve"] continue]
                data: to text! read link
                if not did parse data [
                    to "-----BEGIN PGP PUBLIC KEY BLOCK-----" copy public-key thru "-----END PGP PUBLIC KEY BLOCK-----" 
                    to "-----BEGIN PGP MESSAGE-----" copy message thru "-----END PGP MESSAGE-----" 
                    to end
                ][
                    print {The file you're pointing to is in the wrong format.  It needs to be
-----BEGIN PGP PUBLIC KEY BLOCK-----
newline
your public key data as ascii over many lines
-----END PGP PUBLIC KEY BLOCK-----
newline
-----BEGIN PGP MESSAGE-----
newline
now your registration details encrypted by the server's public key
also over many lines
-----END PGP MESSAGE-----"
}
                    continue
               ]
            ]
            if error? entrap [
                response: write to-url unspaced [chat-server "register"] compose [POST (join "gitlink=" link)]
            ][
                print "Server error occurred - possibly, turning on net-trace"
                net-trace on
                continue
            ] else [
                probe response: to text! response
                if "OK" = copy/part response 2 [
                    print "Looks like we registered"
                    stop
                ]
            ]
        ] else [
            print "This doesn't seem to be a raw Gitlab/GitHub link, try again?"
            continue
        ]
    ]    
]