Rebol [
    title: "Who is registered"
    file: %whois.reb
    notes: {returns a list of registered users}
    date: 29-Feb-2020
]

whois: func [ chat-account [object!]
    <local> response website
][
    if did all [
        website: dirize get in chat-account 'site 
        something? chat-account/sessionkey
    ][
        response: write to url! unspaced [website "whois" ] reduce ['POST (mold chat-account)]
        print "Users registered:"
        print to text! response
    ] else [
        print "You're not logged in yet"
    ]
]