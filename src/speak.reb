Rebol [
    title: "Send a message to chat"
    file: %speak.reb
    date: 24-Feb-2020
    notes: {function to post a chat message to a room}
]

speak: func [ chat-account [object!] content [text! file!]
    /code {wraps the content in a ```Rebol ... ``` for code fencing}
    /replyto [integer!] ; refinements are now their own variables
    <local> website obj response err err2 script tempfile tempfile2
][
    if not integer? chat-account/room [return "No room specified in chat-account object"]
    if file? content [content: unspaced [if code ["```Rebol^/"] to text! read content if code [ "^/```"]]]
  
    if ">>" = copy/part content 2 [
        ; we want to evaluate the following as rebol
        content: skip copy content 2
        ;-- content
        tempfile: to file! remove/part head remove back tail form checksum/method to binary! form now/precise 'md5 2
        ;-- tempfile
        script: unspaced ["Rebol []^/" content]
        ;-- script
        write tempfile script
        tempfile2: join tempfile %.txt
        ;-- tempfile2
        script: spaced ["r3" tempfile]
        ;-- script
        ret: call/output script tempfile2
        ;-- ret
        delete tempfile
        content: unspaced ["```^/>>" content newline to text! read tempfile2 "^/```"]
        delete tempfile2
    ]
  
    
    if get in chat-account 'site [
        website: dirize chat-account/site
        obj: mold make chat-account compose [content: (content) replyto: (replyto: default [_])]
        
        if err: trap [
            load obj
        ][
            if all [did find err/where 'transcode find find/tail content "Rebol" "Rebol"][
                replace find/tail obj "Rebol" "Rebol" "Rebolish"
                if err2: trap [
                    load obj
                ][
                    print "error loading molded message, can not post"
                    probe obj
                    probe err2
                    halt
                ]
            ] else [
                print "error loading molded message, so can not post"
                halt
            ]
        ]

        response: write to url! unspaced [website "rooms/" chat-account/room ] reduce ['POST (obj)]
        probe to text! response
        msg: response
    ] else [
        msg: "No chat-server specified yet.  Maybe you're not logged in?"
        print msg
    ]
    return msg
]
