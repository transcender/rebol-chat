# Instructions

To join the chat we need to confirm who you are.  

## Authentication

* In any publicly accessible github/gitlab site (eg. a gist or even in this [gitlab/rebol-chat/users](https://gitlab.com/Zhaoshirong/rebol-chat/-/tree/master/users) folder) create a file with your `<username>.md`
* At the top of the file put your PGP public key
* Below the public key add an encrypted message with

- password: ...
- full name: ...
- email: ...
- username: ...

in that format with the field names including colons (:) so the chat server can parse out your details and add to the chat system. The contents of the `<username>.md` file should look something like [this](https://gitlab.com/Zhaoshirong/rebol-chat/-/blob/master/users/graham.md).

To produce the encrypted message section, use our [public key](rebolchat.asc) which you need to import. It's under `unspaced ['compkarori '+ 'chat "@gmail.com"]`

```
wget https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/rebolchat.asc
gpg --import rebolchat.asc
```
and then encrypt your registration.txt file like this

```
gpg --recipient compkarori+chat@gmail.com --armor --encrypt registration.txt
```
which produces a file `registration.txt.asc`

If this won't work, you'll just get a silent fail.

Create an issue if you're having problems.


